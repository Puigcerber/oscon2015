# GitLab OSCON Challenge 2015 

## The Details
Create a page like the [OSCON speakers page](http://conferences.oreilly.com/oscon/open-source-eu-2015/public/schedule/speaker/206462) 
for yourself and include why you use and love GitLab and why you’d like to attend OSCON. 
We love creativity, so don’t be shy!

### How to enter

1. Login to GitLab.com and fork this repository.
1. Clone the repository on your workstation.
1. Check out a branch and start working on your masterpiece!
1. Commit your changes and push them to your fork.
1. On GitLab.com create a merge request from your fork to this repository.
1. Send a tweet @GitLab. Include a link to your merge request and use the #OSCON hashtag.

Your submission will ideally be HTML but please include a screenshot in the merge request.
Your merge request and tweet must be sent by midnight PDT on October 22, 2015.

If you have questions about this contest, please create an issue in this repository's
[issue tracker](https://gitlab.com/gitlab-com/oscon2015/issues)

After the submission deadline on October 22nd, GitLab employees will review and vote on all entries to pick the top winner. We’ll send out notifications to the winner by midnight PDT, October 23rd through Twitter.

## The Prize
The winning entry will receive a gold pass to OSCON Amsterdam 2015. This pass includes:

* All sessions (Mon-Tue) 
* All keynotes (Mon-Tue) 
* All tutorials (Wed) 
* Video Compilation (All tutorials, keynotes, and sessions) 
* 3 months of Safari Pro Sponsor Pavilion (Mon-Tue) 
* All on-site networking events Lunch (Mon-Wed)

For more information regarding OSCON Amsterdam 2015, visit OSCON 2015 website.
